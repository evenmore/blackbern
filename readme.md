# BlackBern
Companies selector app written using Javascript and jQuery

## Download and Install
$ git clone https://gitlab.com/evenmore/blackbern.git

$ yarn // or npm install

## Development
Run development page on localhost:8080

$ npm run dev

## Build
Build for production.

$ npm run build


## Project screenshots
![](screenshots/blackbern-example.jpg)
